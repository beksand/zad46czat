package com.beksand;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.time.LocalTime;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Chat {
    public Scanner sc = new Scanner(System.in);
    public Map<LocalTime, String> messages = new HashMap<>();

    public Chat() {
    }

    public void sendMessage(){
        try {
            PrintWriter message = new PrintWriter(new FileOutputStream("message.txt", true));
            message.println(LocalTime.now()+" "+sc.nextLine());
            message.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
    public void getMessage(){
        File message = new File("message.txt");
        try {
            Scanner scMessage = new Scanner(message);
            while (scMessage.hasNext()) {
                String messageline = scMessage.nextLine();
//                messages.put(LocalTime.now(), messageline);
                System.out.println(messageline);
            }
            scMessage.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Override
    public String toString() {
        return "Chat{" +
                "messages=" + messages +
                '}';
    }
}
