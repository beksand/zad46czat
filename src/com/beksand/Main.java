package com.beksand;

//Utwórz klasę umożliwiającą komunikowanie się pomiędzy użytkownikami. Stwórz klasę: com.beksand.Chat, która
//        będzie miała metody sendMessage i getMessage. sendMessage zapisze dane od użytkownika, getMessage pokaże
//        użytkownikowi to co jest w pliku. Odpalimy dwie instancje programu i spróbujemy
//        wyświetlać i pisać do obu programów. Pobranie danych i ich wyświetlenie następuje po enterze.
//        * Spróbujcie wyświetlić tylko najnowsze dane.



public class Main {

    public static void main(String[] args) {
        Chat chat = new Chat();
        while (true){
            chat.sendMessage();
            chat.getMessage();
        }
    }
}
